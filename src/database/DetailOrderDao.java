/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author hafish
 */
public class DetailOrderDao {

    public static boolean insert(DetailOrder orderDetail) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO [DetailOrder] (\n"
                    + "                     orderId,\n"
                    + "                     productId,\n"
                    + "                     amount,\n"
                    + "                     total)\n"
                    + "                 VALUES (\n"
                    + "                     '%d',\n"
                    + "                     '%d',\n"
                    + "                     '%d',\n"
                    + "                     '%f');";

//            stm.execute(String.format(sql,
//                    orderDetail.getOrderId(),
//                    orderDetail.getProduct().getProductId(),
//                    orderDetail.getAmount(),
//                    orderDetail.getTotal()
//            ));
            Database.close();
            return true;
        } catch (SQLException ex) {

        }
        Database.close();
        return true;
    }

    public static ArrayList<DetailOrder> getOrderDetails() {
        ArrayList<DetailOrder> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * "
                    + "  FROM DetailOrder";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                DetailOrder orderDetail = toObject(rs);
                list.add(orderDetail);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }
    
    public static DetailOrder getOrderDetails(int orderId) {
        ArrayList<DetailOrder> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * "
                    + "  FROM DetailOrder where orderId = "+orderId;
            ResultSet rs = stm.executeQuery(sql);
            Database.close();
            while (rs.next()) {
                DetailOrder orderDetail = toObject(rs);
                return orderDetail;
            }
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }
    
    
    private static DetailOrder toObject(ResultSet rs) throws SQLException {
        DetailOrder orderDetail = null;
//        int productId = ProductDao.getProducts(rs.getInt("productId")) ;
//        orderDetail = new OrderDetail(
//                rs.getInt("orderDetailId"),
//                rs.getInt("orderId"),
//                
//                rs.getInt("amount"),
//                rs.getDouble("total")
//        );
        return orderDetail;
    }
}
