/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author werap
 */
public class OrderDao {

    public static boolean insert(Order order) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO [Order] (\n"
                    + "                     staffId,\n"
                    + "                     memberId,\n"
                    + "                     amount,\n"
                    + "                     price,\n"
                    + "                     discount,\n"
                    + "                     total)\n"
                    + "                 VALUES (\n"
                    + "                     '%d',\n"
                    + "                     '%d',\n"
                    + "                     '%d',\n"
                    + "                     '%f',\n"
                    + "                     '%f',\n"
                    + "                     '%f');";

            stm.execute(String.format(sql,
                    order.getStaffId(),
                    order.getMemberId(),
                    order.getAmount(),
                    order.getPrice(),
                    order.getDiscount(),
                    order.getTotal()
            ));
            Database.close();
            return true;
        } catch (SQLException ex) {

        }
        Database.close();
        return true;
    }

    public static boolean delete(Order order) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "DELETE FROM order\n"
                    + "WHERE orderId = %d";
            String sqlFormat = String.format(sql,
                    order.getOrderId());

            stm.executeQuery(sqlFormat);
            Database.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static ArrayList<Order> getOrders() {
        ArrayList<Order> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * "
                    + "  FROM order";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Order order = toObject(rs);
                list.add(order);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    public static Order getOrder(int userId) {
        Database.conn = Database.connect();
        try {
            Statement stm = Database.conn.createStatement();
            String sql = "SELECT * FROM user WHERE userId = %d";
            ResultSet rs = stm.executeQuery(String.format(sql, userId));
            while (rs.next()) {
                Order order = toObject(rs);
                Database.close();
                return order;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public static ArrayList<Sale> showSaleDay() {
        ArrayList<Sale> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select \n" +
                        "strftime('%d-%m-%Y', dateTime) as date,\n" +
                        "sum(amount) as amount,\n" +
                        "sum(Total) as price\n" +
                        "from [order]\n" +
                        "group by date\n" +
                        "order by date DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sale sale = toObjectSale(rs);
                list.add(sale);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;

    }

    public static ArrayList<Sale> showSaleMonth() {
        ArrayList<Sale> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select \n" +
                        "strftime('%m-%Y', dateTime) as date,\n" +
                        "sum(amount) as amount,\n" +
                        "sum(Total) as price\n" +
                        "from [order]\n" +
                        "group by date\n" +
                        "order by date DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sale sale = toObjectSale(rs);
                list.add(sale);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;

    }

    public static ArrayList<Sale> showSaleYears() {
        ArrayList<Sale> list = new ArrayList(); 
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select \n" +
                        "strftime('%Y', dateTime) date,\n" +
                        "sum(amount) as amount,\n" +
                        "sum(Total) as price\n" +
                        "from [order]\n" +
                        "group by date\n" +
                        "order by date DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sale sale = toObjectSale(rs);
                list.add(sale);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;

    }

    public static ArrayList<Sale> showSale() {
        ArrayList<Sale> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select \n" +
                        "strftime('%d-%m-%Y %H:%M:%S', dateTime) as date,\n" +
                        "amount ,\n" +
                        "Total as price\n" +
                        "from [order]\n" +
                        "order by date DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sale sale = toObjectSale(rs);
                list.add(sale);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;

    }

    public static void sumTotal() {

    }

    private static Order toObject(ResultSet rs) throws SQLException {
        Order order;
        order = new Order(
                rs.getInt("orderId"),
                rs.getInt("staffId"),
                rs.getInt("memberId"),
                rs.getInt("amount"),
                rs.getDouble("price"),
                rs.getDouble("discount"),
                rs.getDouble("total"),
                rs.getString("datetime"),
                rs.getDouble("getTotal"),
                rs.getDouble("giveTotal")
        );
        return order;
    }
    
     private static Sale toObjectSale(ResultSet rs) throws SQLException {
        Sale sale;
        sale = new Sale(
                rs.getString("date"),
                rs.getInt("amount"),
                rs.getInt("price")
        );
        return sale;
    }
    
}
