/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author User
 */
public class Product {

    private int productId;
    private String name;
    private String model;
    private double price;
    private int amount;

    public Product(int productId,String name,String model,double price,int amount) {
        this.productId = productId;
        this.name = name;
        this.model = model;
        this.price = price;
        this.amount = amount;
    }
    public Product(){
        
    }

    public int getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Product{" + "productId=" + productId + ", name=" + name + ", model=" + model + ", price=" + price + ", amount=" + amount + '}';
    }

}
