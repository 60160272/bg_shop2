/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableModel;

import database.Member;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author mongkhon
 */
public class MemberTableModel extends AbstractTableModel {
    ArrayList<Member> memberList = new ArrayList<Member>();
    String[] columnNames = {"ลำดับ", "ชื่อ", "นามสกุล", "เบอร์โทร"};
    @Override
    public int getRowCount() {
        return memberList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Member member = memberList.get(rowIndex);
        switch(columnIndex) {
            case 0: return member.getMemberId();
            case 1: return member.getFirstname();
            case 2: return member.getSurname();
            case 3: return member.getPhone();
        }
        return "";
    }   
    @Override
    public String getColumnName(int column){
        return columnNames[column];
    }
    
    public void setData(ArrayList<Member> memberList) {
        this.memberList = memberList;
        fireTableDataChanged();
    }
}
